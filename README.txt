===============================================================================
© Copyright 2018 Yoann MOUGNIBAS

This file is part of Base-Maven.

Base-Maven is free software: you can redistribute it and/or modif
it under the terms of the GNU General Public License as published
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Base-Maven is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Base-Maven. If not, see <http://www.gnu.org/licenses/>
===============================================================================

A project to handle common maven plugins version.

To sign with pgp, add this profile (profiles section) to your maven user settings ($HOME/.m2/settings.xml) :
		<profile>
			<id>gpg-config</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<properties>
				<gpg.passphrase><![CDATA[your passphrase with <weird> characters]]></gpg.passphrase>
			</properties>
		</profile>


To deploy, the ossrh serveur must be defined in the maven user settings ($HOME/.m2/settings.xml) :
<settings>
  <servers>
    <server>
      <id>ossrh</id>
      <username>your-jira-id</username>
      <password>your-jira-pwd</password>
    </server>
  </servers>
</settings>
See http://central.sonatype.org/pages/apache-maven.html

Build :
mvn clean package

Install :
mvn clean install

Install (without signing) :
mvn clean install -Dgpg.skip=true

Deploy to ossrh :
mvn clean deploy

Deploy to central :
Login to https://oss.sonatype.org/
In "Staging Repository", Select the repository to deploy (frmougnibasbase-maven-1xxxx), then "Close" button.
Once the repository is closed, hit the "Release" button to release to maven central.